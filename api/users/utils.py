#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from datetime import datetime

import aiocouch
from _common.utils import make_id
from config.config import Cdb, couchdb
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder

logger = logging.getLogger("default")


async def get_by_username(username):
    """
    Get user by username
    """

    logger.debug(f"- user.utils get_by_username for user: {username}")
    db = await couchdb[Cdb.USERS.value]

    try:
        return await db[username]
    except aiocouch.exception.NotFoundError:
        # NOTICE: we cannot raise exception here as downstream (ldap auth check) needs to account for no user found (so it can create).
        return None


async def add_user(user=None):
    """
    Add user
    """

    db = await couchdb[Cdb.USERS.value]

    user.created = datetime.now()
    user.updated = datetime.now()

    try:
        doc = await db.create(
            make_id(items=[user.username]), data=jsonable_encoder(user)
        )
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(
            status_code=409, detail=f"Duplicate Key: cannot add '{user}'."
        )


async def update_user(user=None):
    """
    Update user
    """

    user.updated = datetime.now()

    try:
        doc = await get_by_username(user.username)
        doc["enabled"] = user.enabled
        doc["note"] = user.note
        doc["accounts"] = user.accounts
        doc["first_name"] = user.first_name
        doc["last_name"] = user.last_name
        doc["display_name"] = user.display_name
        doc["email"] = user.email
        doc["avatar"] = user.avatar
        await doc.save()

        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(
            status_code=409, detail=f"Duplicate Key: cannot add '{user}'."
        )
