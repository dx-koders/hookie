#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id
from config.config import couchdb
from fastapi import APIRouter, Depends, HTTPException, Query, Security
from fastapi.encoders import jsonable_encoder
from users.models import (
    User,
    UserAccount,
    UserBase,
    _db,
    get_current_active_user,
    get_current_user,
)

logger = logging.getLogger("default")
users_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@users_router.get("/me", response_model=UserBase)
async def me(current_user: UserBase = Depends(get_current_active_user)):
    """Get myself"""
    return current_user


@users_router.get("/", response_model=List[User])
async def get_all(
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    """

    # @TODO: add limit and skip logic.
    db = await couchdb[_db.value]

    docs = []
    async for doc in db.docs():
        docs.append(fix_id(doc))

    return docs


# NOTICE: we do not need an 'add' route in api as users are added through
#   ldap; however, there is a non-exposed add in utils.py.


@users_router.get("/{username}", response_model=User)
async def get_one(
    username: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """Get by username"""

    db = await couchdb[_db.value]

    return fix_id(await _get_or_404(username, db))


# 2020-07-01:drad update of user is limited to updating the following as other items are set via ldap:
#   - disabled: this will disable the user in the app
#   - note: an app specific note for the user
#   - accounts: a list of accounts the user belongs to
@users_router.put(
    "/{username}",
    response_model=dict,
)
async def update(
    username: str,
    enabled: bool = Query(True, description="Is the user enabled?"),
    note: str = Query(None, description="note on user"),
    accounts: List[UserAccount] = None,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Update
    """

    db = await couchdb[_db.value]
    doc = await _get_or_404(username, db)
    # @TODO currently dont allow update of account and name for now (need to update pk)
    doc["enabled"] = enabled
    doc["note"] = note if note else doc["note"]
    doc["accounts"] = jsonable_encoder(accounts)
    await doc.save()

    return await doc.info()


@users_router.delete("/{username}", response_model=dict)
async def delete(
    username: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Delete by id
    """

    resp = DocInfo(ok="ok", id=username, rev="", msg="deleted")
    db = await couchdb[_db.value]
    doc = await _get_or_404(username, db)
    dr = await doc.delete()
    # note: dr will be None if delete succeeds.
    if dr:
        resp.ok = "fail"
        resp.msg = dr

    return resp
