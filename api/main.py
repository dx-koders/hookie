#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>
#
# NOTICE: logging is designed to run at INFO loglevel.

import logging

from _common.utils import check_databases
from accounts.routes import accounts_router
from auth.routes import auth_router
from config.config import (
    API_BASE_PATH,
    API_JWT_EXPIRE_DAYS,
    API_JWT_EXPIRE_HOURS,
    API_JWT_EXPIRE_MINUTES,
    API_PATH,
    API_RC_CHANNEL,
    API_RC_NOTIFY_ENABLED,
    API_RC_SERVER_URL,
    APP_LOGLEVEL,
    CDB_URI,
    CORS_ORIGINS,
    DEPLOY_ENV,
    LOG_TO,
    cfg,
    couchdb,
)
from core.routes import core_router
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from hooks.routes import hooks_router
from payloads.routes import payloads_router
from users.routes import users_router

logger_base = logging.getLogger("default")
logger_base.setLevel(logging.getLevelName(APP_LOGLEVEL))
console_handler = logging.StreamHandler()
if "console" in LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("default"),
    {"application_name": cfg.core.name, "application_env": DEPLOY_ENV},
)

logger.info(
    f"{cfg.core.name} - v.{cfg.core.version} ({cfg.core.modified}) - {APP_LOGLEVEL} - {LOG_TO}"
)

logger.info(f"  ✦ Log Level:     {APP_LOGLEVEL}")
logger.info(f"  ✦ Log To:        {LOG_TO}")
logger.info(f"  ✦ Deploy Env:    {DEPLOY_ENV}")
logger.info(f"  ✦ CDB URI:       {CDB_URI}")
logger.info(f"  ✦ API Base Path: {API_PATH}")
logger.info(f"  ✦ CORS Origins:  {CORS_ORIGINS}")
logger.info(
    f"  ✦ JWT Expire:    {API_JWT_EXPIRE_DAYS}:{API_JWT_EXPIRE_HOURS}:{API_JWT_EXPIRE_MINUTES} (Days:Hours:Minutes)"
)
if API_RC_NOTIFY_ENABLED:
    logger.info(
        f"  ✦ RC Notify:     enabled --> {API_RC_SERVER_URL} (channel: {API_RC_CHANNEL})"
    )

app = FastAPI(
    title=f"{cfg.core.name}",
    description=f"{cfg.core.description}",
    version=f"{cfg.core.version}",
    openapi_url=f"{API_PATH}/openapi.json",
    docs_url=f"{API_PATH}/docs",
    redoc_url=None,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=CORS_ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(
    accounts_router,
    prefix=f"{API_PATH}/accounts",
    tags=["accounts"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    hooks_router,
    prefix=f"{API_PATH}/hooks",
    tags=["hooks"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    payloads_router,
    prefix=f"{API_PATH}/payloads",
    tags=["payloads"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    users_router,
    prefix=f"{API_PATH}/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    core_router,
    prefix=f"{API_BASE_PATH}",
    tags=["core"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    auth_router,
    prefix=f"{API_PATH}/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)


@app.on_event("startup")
async def app_startup():
    await couchdb.check_credentials()
    await check_databases()
    pass


@app.on_event("shutdown")
async def app_shutdown():
    await couchdb.close()
