#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import inspect
import logging
import uuid
from datetime import datetime
from typing import List

import aiocouch
import arrow
from _common.models import DocInfo
from _common.utils import fix_id, get_route_roles
from accounts.routes import check_access_get_db
from config.config import API_RC_NOTIFY_ENABLED, DEPLOY_ENV
from fastapi import APIRouter, Body, HTTPException, Path, Query, Security, status
from fastapi.encoders import jsonable_encoder
from payloads.models import Payload, PayloadExt, _db
from payloads.utils import format_event, notify_via_rocketchat
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
payloads_router = APIRouter()


async def _get_or_404(_id, db, _all: bool = False):
    """
    Get a document or raise a 404
    """

    # @TODO: I think we need to change this to get_or_none and update logic accordingly.
    #   ^-- dont want to do this if we dont have to...
    logger.debug(f"_get_or_404 request on: {_id} with all: {_all}")
    try:
        doc = await db[_id]
        if _all is False and not doc["enabled"]:
            raise HTTPException(status_code=404, detail="Not found")
        return doc

    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")
        # ~ logger.debug(f"- _get_or_404 didn't find anything, returning None")
        # ~ return None


@payloads_router.get("/{_id}", response_model=Payload)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    logger.debug("- payloads get_one...")

    db = await check_access_get_db(
        account=_id.split(":")[0],
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    doc = fix_id(await _get_or_404(_id, db))
    logger.debug("- get_one returning doc: {doc}")

    return doc


@payloads_router.get("/", response_model=List[Payload])
async def get_all(
    account: str,
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all for account
    - shows only enabled payloads unless user is admin
    """

    # @TODO: add limit and skip logic.
    db = await check_access_get_db(
        account=account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    is_admin = "admins" in current_user.roles

    # get for given account (only include enabled and available_units != 0)
    selector = {"_id": {"$regex": f"^{account}:.*$"}, "enabled": True}
    # if user is admin they get disabled and 0 unit payloads to be able to edit them.
    if is_admin:
        selector = {"_id": {"$regex": f"^{account}:.*$"}}
    docs = []
    async for doc in db.find(selector=selector):
        docs.append(fix_id(doc))

    return docs


@payloads_router.post(
    "/{hook}", response_model=DocInfo, status_code=status.HTTP_201_CREATED
)
async def add(
    hook: str = Path(..., description="The id of the hook the payload belongs to"),
    data: dict = Body(...),
    # ~ current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new
    """

    # ~ db = await check_access_get_db(
    # ~ account=hook.split(":")[0],
    # ~ current_user=current_user,
    # ~ roles=get_route_roles(inspect.currentframe()),
    # ~ db=_db,
    # ~ )
    from config.config import couchdb

    db = await couchdb[_db.value]

    logger.debug(f"- received payload data of: {data}")

    payload = PayloadExt(
        hook=hook,
        data=data,
        created=datetime.now(),
        updated=datetime.now(),
    )

    try:
        doc = await db.create(str(uuid.uuid4()), data=jsonable_encoder(payload))
        await doc.save()

        r = await doc.info()
        # if we were successful in adding the point then send notification.
        # determine time.
        event_type = None
        event_data = None
        if "event_type" in data:
            event_type = data["event_type"]
            event_data = data["events"][0]
        elif "type" in data:
            event_type = data["type"]
            event_data = data["event_data"]

        event_data = await format_event(event_type=event_type, data=event_data)
        if r["ok"] and API_RC_NOTIFY_ENABLED:
            deploy_env = ""
            # show environment flag if environment is not prod
            if DEPLOY_ENV != "prd":
                deploy_env = " - `" + DEPLOY_ENV.upper() + "`"
            await notify_via_rocketchat(
                f"""
**Hook: {hook}**{deploy_env}
{arrow.get(payload.created).format('YYYY-MM-DD HH:mm')} / {event_type}
{event_data}
"""
            )

        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(
            status_code=409, detail=f"Duplicate Key: cannot add '{hook}'."
        )


# ~ @payloads_router.put("/", response_model=DocInfo)
# ~ async def update(
# ~ hook: str = Query(..., description="The id of the hook the payload belongs to"),
# ~ data: str = Query(..., description="The payload data"),

# ~ current_user: UserBase = Security(get_current_user, scopes=["users"]),
# ~ ):
# ~ """
# ~ Update
# ~ """

# ~ db = await check_access_get_db(
# ~ account=_id.split(":")[0],
# ~ current_user=current_user,
# ~ roles=get_route_roles(inspect.currentframe()),
# ~ db=_db,
# ~ )

# ~ doc = await _get_or_404(_id, db, True)
# ~ doc["hook"] = hook if hook else doc["hook"]
# ~ doc["data"] = data if data else doc["data"]
# ~ doc["updated"] = jsonable_encoder(datetime.now())
# ~ await doc.save()

# ~ return await doc.info()


@payloads_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str = Query(..., description="the id of the payload to delete"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Delete by id
    """

    db = await check_access_get_db(
        account=_id.split(":")[0],
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    resp = DocInfo(ok="ok", id=_id, rev="", msg="deleted")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    # note: dr will be None if delete succeeds.
    if dr:
        resp.ok = "fail"
        resp.msg = dr

    return resp
