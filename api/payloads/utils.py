#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging

from config.config import (
    API_RC_CHANNEL,
    API_RC_SERVER_URL,
    API_RC_TOKEN,
    API_RC_USER_ID,
    cfg,
)
from requests import sessions
from rocketchat_API.rocketchat import RocketChat

logger = logging.getLogger("default")


async def format_event(event_type: str = "SCANNING_COMPLETED", data: str = ""):
    """
    Format an event for notification delivery.
    """

    logger.debug(f"- formatting event of type: {event_type}")
    ret = ""
    if event_type == "SCANNING_COMPLETED":
        logger.debug("- match on SCANNING_COMPLETED")
        repo = data["repository"]["repo_full_name"]
        sdata = data["resources"][0]["scan_overview"][
            "application/vnd.security.vulnerability.report; version=1.1"
        ]
        severity = (
            f"**{sdata['severity']}**"
            if sdata["severity"] == "Critical"
            else sdata["severity"]
        )
        total = sdata["summary"]["total"]
        fixable = sdata["summary"]["fixable"]
        critical = (
            f"**{sdata['summary']['summary']['Critical']}**"
            if "Critical" in sdata["summary"]["summary"]
            else "0"
        )
        high = (
            sdata["summary"]["summary"]["High"]
            if "High" in sdata["summary"]["summary"]
            else "0"
        )
        medium = (
            sdata["summary"]["summary"]["Medium"]
            if "Medium" in sdata["summary"]["summary"]
            else "0"
        )
        low = (
            sdata["summary"]["summary"]["Low"]
            if "Low" in sdata["summary"]["summary"]
            else "0"
        )
        duration = sdata["duration"]

        ret = f"""
+ repo: {repo} {severity}
+ total: {total} / fixable: {fixable} / duration: {duration}s
+ critical: {critical} / high: {high} / medium: {medium} / low: {low}
"""
    else:
        # if we do not have a formatter we return the original data.
        logger.debug("- formatter not found, returning supplied data")
        ret = data

    logger.debug(f"- formatting result: {ret}")

    return ret


async def notify_via_rocketchat(message: str = ""):
    """
    Notifiy via rocketchat
    """

    logger.debug(f"- notifying via rocketchat, server: {API_RC_SERVER_URL}")
    with sessions.Session() as session:
        rocket = RocketChat(
            user_id=API_RC_USER_ID,
            auth_token=API_RC_TOKEN,
            server_url=API_RC_SERVER_URL,
            session=session,
        )
        r = rocket.chat_post_message(
            message,
            channel=API_RC_CHANNEL,
            alias=cfg.core.name,
        ).json()
        logger.debug(f"  --> rocketchat post_message response: {r}")

    return True
