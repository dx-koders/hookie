#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from datetime import datetime

from config.config import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.PAYLOADS


class PayloadBase(BaseModel):
    """the base"""

    hook: str = None  # id of the hook the payload belongs to
    data: dict = None  # the payload data


class PayloadExt(PayloadBase):
    """Extended (added by backend logic)"""

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()


class Payload(PayloadExt):
    """Actual (at DB level)"""

    id_: str
