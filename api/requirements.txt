aiocouch==2.2.2                                                         # couchdb async driver
aiohttp==3.8.4                                                          # needed for arq-worker and async http requests to couchdb
arrow==1.2.3                                                            # better date handling
email-validator==2.0.0.post2                                            # required for EmailStr data type
fastapi==0.95.2                                                         # fastapi
ldap3==2.9.1                                                            # ldap authentication
Markdown==3.4.3                                                         # markdown to html converter used to send email/rocketchat notifications
python-multipart==0.0.6                                                 # needed for fastapi depends() (for auth)
pyjwt==2.7.0                                                            # needed for auth (jwt tokens)
requests==2.31.0
rocketchat_api==1.30.0                                                  # send rocketchat notifications
uvicorn==0.22.0                                                         # wsgi server
