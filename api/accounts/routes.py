#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from datetime import datetime
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id, make_id
from accounts.models import Account, AccountExt, _db
from auth.utils import has_account_access
from config.config import couchdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
accounts_router = APIRouter()


async def check_access_get_db(
    account: str = "", current_user: UserBase = "", roles: list = [], db=None
):
    """
    Check access and return appropriate db
    Return: tasks db object
    """

    logger.debug(
        f"- checking access & get db for:\n  + account={account}\n  + current user: {current_user}\n  + route roles: {roles}\n  + db: {db.value}"
    )
    acc = await get_account(account)
    logger.debug(f"- found account: {acc}")
    if not await has_account_access(
        current_user=current_user, account=acc, roles=roles
    ):
        raise HTTPException(status_code=401, detail="Not authorized")

    return await couchdb[db.value]


async def _get_or_404(_id):
    """
    Get document by id or raise a 404
    """

    logger.debug(f"_get_or_404 request on: {_id.lower()}")
    db = await couchdb[_db.value]
    try:
        doc = fix_id(await db[_id.lower()])
        logger.debug(f"  - found doc: {doc}")
        return doc
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


async def get_account(name: str = None) -> Account:
    """
    Get account by name
    """

    logger.debug(f"- getting account of name: {name}")
    doc = await _get_or_404(name)
    return Account(**doc)


@accounts_router.get("/", response_model=List[Account])
async def get_all(
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    """

    # @TODO: add limit and skip logic.
    db = await couchdb[_db.value]

    docs = []
    async for doc in db.docs():
        docs.append(fix_id(doc))

    return docs


@accounts_router.get("/{_id}", response_model=Account)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get one by id
    """

    logger.debug(f"- get one by id: {_id}")

    return await _get_or_404(_id)


@accounts_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    name: str = Query(..., description="The name of the account"),
    note: str = Query(None, description="Notes about the account"),
    enabled: bool = Query(True, description="Is the account enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Add new
    """

    db = await couchdb[_db.value]

    doc = AccountExt(
        name=name,
        enabled=enabled,
        note=note,
        created=datetime.now(),
        updated=datetime.now(),
    )

    try:
        doc = await db.create(make_id(items=[name]), data=jsonable_encoder(doc))
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(
            status_code=409, detail=f"Duplicate Key: cannot add '{name}'."
        )


@accounts_router.put(
    "/{id_}",
    response_model=DocInfo,
)
async def update(
    _id: str,
    name: str = Query(..., description="The name of the account"),
    note: str = Query(None, description="Notes about the account"),
    enabled: bool = Query(True, description="Is the account enabled?"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Update
    """

    doc = await _get_or_404(_id)
    # @TODO currently dont allow update of account and name for now (need to update pk)
    doc["name"] = name if name else doc["name"]
    doc["enabled"] = enabled
    doc["note"] = note if note else doc["note"]
    await doc.save()

    return await doc.info()


@accounts_router.delete("/{id}", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Delete by id
    """

    resp = DocInfo(ok="ok", id=_id, rev="", msg="deleted")
    doc = await _get_or_404(_id)
    dr = await doc.delete()
    # note: dr will be None if delete succeeds.
    if dr:
        resp.ok = "fail"
        resp.msg = dr

    return resp
