#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from datetime import datetime
from typing import Optional

from config.config import Cdb
from pydantic import BaseModel

logger = logging.getLogger("default")
_db = Cdb.HOOKS


class HookBase(BaseModel):
    """the base"""

    account: str = None  # id of the account to which item belongs
    name: str = None
    enabled: bool = True
    description: Optional[str] = None
    # payload: Json = None
    # value: float = 1
    # icon: Optional[str] = None
    # usage_count: Optional[int] = 0
    # available_units: Optional[int] = None


class HookExt(HookBase):
    """Extended (added by backend logic)"""

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()
    updated: datetime = datetime.now()


class Hook(HookExt):
    """Actual (at DB level)"""

    id_: str
