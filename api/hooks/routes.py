#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import inspect
import logging
from datetime import datetime
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id, get_route_roles, make_id
from accounts.routes import check_access_get_db
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from hooks.models import Hook, HookExt, _db
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
hooks_router = APIRouter()


async def _get_or_404(_id, db, _all: bool = False):
    """
    Get a document or raise a 404
    """

    # @TODO: I think we need to change this to get_or_none and update logic accordingly.
    #   ^-- dont want to do this if we dont have to...
    logger.debug(f"_get_or_404 request on: {_id} with all: {_all}")
    try:
        doc = await db[_id]
        if _all is False and not doc["enabled"]:
            raise HTTPException(status_code=404, detail="Not found")
        return doc

    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")
        # ~ logger.debug(f"- _get_or_404 didn't find anything, returning None")
        # ~ return None


@hooks_router.get("/{_id}", response_model=Hook)
async def get_one(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    logger.debug("- hooks get_one...")

    db = await check_access_get_db(
        account=_id.split(":")[0],
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    doc = fix_id(await _get_or_404(_id, db))
    logger.debug("- get_one returning doc: {doc}")

    return doc


@hooks_router.get("/", response_model=List[Hook])
async def get_all(
    account: str,
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all for account
    - shows only enabled hooks unless user is admin
    """

    # @TODO: add limit and skip logic.
    db = await check_access_get_db(
        account=account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    is_admin = "admins" in current_user.roles

    # get for given account (only include enabled and available_units != 0)
    selector = {"_id": {"$regex": f"^{account}:.*$"}, "enabled": True}
    # if user is admin they get disabled and 0 unit hooks to be able to edit them.
    if is_admin:
        selector = {"_id": {"$regex": f"^{account}:.*$"}}
    docs = []
    async for doc in db.find(selector=selector):
        docs.append(fix_id(doc))

    return docs


@hooks_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    account: str = Query(..., description="The account of the hook"),
    name: str = Query(..., description="The name of the hook"),
    description: str = Query(None, description="A description of the hook"),
    enabled: bool = Query(True, description="Is the hook enabled?"),
    # payload: Json = Query(None, description="Webhook payload"),
    # value: float = Query(..., description="The value of the hook"),
    # icon: str = Query(None, description="The icon for the hook"),
    # available_units: int = Query(
    #    None, description="Number of units available (leave blank for unlimited)"
    # ),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new
    """

    db = await check_access_get_db(
        account=account,
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    hook = HookExt(
        account=account,
        name=name,
        # value=value,
        enabled=enabled,
        description=description,
        # payload=payload,
        # icon=icon,
        # available_units=available_units,
        created=datetime.now(),
        updated=datetime.now(),
    )

    try:
        doc = await db.create(
            make_id(items=[account, name]), data=jsonable_encoder(hook)
        )
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(
            status_code=409, detail=f"Duplicate Key: cannot add '{name}'."
        )


@hooks_router.put("/", response_model=DocInfo)
async def update(
    _id: str,
    # account: str,  # no update of account
    name: str = Query(None, description="Name of hook"),
    description: str = Query(None, description="A description of the hook"),
    # value: float = Query(None, description="The value of the hook"),
    # icon: str = Query(None, description="The icon for the hook"),
    enabled: bool = Query(True, description="Is the hook enabled?"),
    # available_units: int = Query(
    #    None, description="Number of units available (leave blank for unlimited)"
    # ),
    # payload: Json = Query(None, description="Webhook payload"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update
    """

    db = await check_access_get_db(
        account=_id.split(":")[0],
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    doc = await _get_or_404(_id, db, True)
    doc["name"] = name if name else doc["name"]
    # doc["value"] = value if value else doc["value"]
    # doc["icon"] = icon if icon else doc["icon"]
    doc["enabled"] = enabled
    # doc["available_units"] = available_units
    doc["description"] = description if description else doc["description"]
    # doc["payload"] = payload if payload else doc["payload"]
    doc["updated"] = jsonable_encoder(datetime.now())
    await doc.save()

    return await doc.info()


@hooks_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str = Query(..., description="the id of the hook to delete"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Delete by id
    """

    db = await check_access_get_db(
        account=_id.split(":")[0],
        current_user=current_user,
        roles=get_route_roles(inspect.currentframe()),
        db=_db,
    )

    resp = DocInfo(ok="ok", id=_id, rev="", msg="deleted")
    doc = await _get_or_404(_id, db)
    dr = await doc.delete()
    # note: dr will be None if delete succeeds.
    if dr:
        resp.ok = "fail"
        resp.msg = dr

    return resp
