#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

from datetime import date
from enum import Enum
from typing import Optional

from pydantic import BaseModel


class Cdb(Enum):
    """
    Available databases
    """

    ACTIVITIES = "activities"
    ACTIVITY_MEDIA = "activity_media"
    LOGS = "logs"
    RELATED_ITEM_OVERVIEWS = "related_item_overviews"
    RELATED_ITEM_TYPES = "related_item_types"
    USERS = "users"
    USER_MESSAGES = "user_messages"


class ConfigApiVersions(BaseModel):
    """
    API Versions.
    """

    current: str = None
    supported: str = None
    deprecated: str = None
    unsupported: str = None


class ConfigApiJwt(BaseModel):
    """
    API JWT config.
    """

    algorithm: str = None


class ConfigRouteAuthItem(BaseModel):
    """
    Route Auth Item.
    """

    # get: Optional[list] = None
    get_all: Optional[list] = None
    get_one: Optional[list] = None
    add: Optional[list] = None
    update: Optional[list] = None
    delete: Optional[list] = None


class ConfigRouteAuth(BaseModel):
    """
    Route Auth config.
    """

    accounts: ConfigRouteAuthItem = None
    notifiers: ConfigRouteAuthItem = None
    hooks: ConfigRouteAuthItem = None
    payloads: ConfigRouteAuthItem = None


class ConfigApi(BaseModel):
    """
    API specific config.
    """

    versions: ConfigApiVersions = None
    jwt: ConfigApiJwt = None
    # NOTE: route_auth is not parsed as it is used by inspect.frame which is easier as a raw dict.
    route_auth: dict = None


class ConfigCore(BaseModel):
    """
    Core specific config.
    """

    name: str = None
    description: str = None
    version: str = None
    created: date = None
    modified: date = None


class ConfigBase(BaseModel):
    """
    Config Base.
    """

    api: ConfigApi = None
    core: ConfigCore = None
