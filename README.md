# README

Hookie is a webhook proxy!

###  Goal  ###



###  More Info  ###
See the `docs` directory.


### Setup ###

- create a virtual environment in your python root: `python3 -m virtualenv .venv`
- activate the virtualenv: `source .env-setup`
- install packages: `pip install -r requirements.txt`
