# DEVELOPMENT

Information about development for this project.

## App Setup ##

You need to setup the initial 'root' user:

- log in as a user who is in the 'admin' group for your app (e.g. 'hookie_admins')
    + this will create the user in the db from the data returned from LDAP
- create a new account in the api (`/api/v2/accounts/ add`), making sure it is enabled
- update the user in the api (`/api/v2/users/{username} Update`) to give them 'admin' role for the account created above


##  Developing  ##

###  Pre-Commit Info  ###

- exclude line for detect-secrets: `# pragma: allowlist secret`

#### Setup ####

- install pre-commit: `pip install pre-commit`
- init pre-commit for project: `pre-commit install`
- use: perform a commit!


##  Code Wrapup  ##
After coding is complete there are a few general tasks which should be performed:

- run bandit (currently not in pre-commit hook as it has issues with black)
- ensure the following files are synced:
    - Dockerfile*
    - docker-compose*
    - .env*
    - configmap.yml*
    - deployment-*
    - no sensitive information left in code, examples, or docs
    - increment app version accordingly

##  Releases  ##

K8s releases are rolled out (we use rancher but any mechanism will work) from the built image.


## Tips and Useful Info ##

- ???

### Links ###

- [fastapi](https://fastapi.tiangolo.com/)
- [rocketchat_API](https://github.com/jadolg/rocketchat_API)
    + [rocketchat REST API](https://developer.rocket.chat/api/rest-api)
- [harbor webhooks](https://goharbor.io/docs/1.10/working-with-projects/project-configuration/configure-webhooks/)
    + NOTICE: the webhook example is wrong (not current), see the `test/data` directory for real/current examples
