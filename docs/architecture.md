# Architecture

###  Stack  ###

The application stack is as follows:
- api: backend application
    - fastapi: core application
    - uvicorn/gunicorn: http/wsgi interface to application
    - ldap3: ldap authentication
- db: couchdb


###  Authorization Scheme  ###

See the authorization_scheme.ods spreadsheet for details related to authorization including a breakout of privileges by Method for each Model in the application.

###  Links  ###

- [fastapi](https://fastapi.tiangolo.com/)


